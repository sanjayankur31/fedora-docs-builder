Name:           fedora-docs-builder
Version:        1.0.0
Release:        %autorelease
Summary:        Builder script for Fedora Antora based documentation

License:        CC0
URL:            https://gitlab.com/fedora/docs/templates/fedora-docs-template
Source0:        %{url}/-/raw/main/builder.sh
# The repo is under CC0, but CC0 is no longer appropriate for code, so we may
# need to change this
# https://gitlab.com/fedora/docs/templates/fedora-docs-template/-/issues/5
Source1:        %{url}/-/raw/main/LICENSE

BuildArch:      noarch

Requires:       git-core
Requires:       inotify-tools
Requires:       podman
Requires:       python3
Requires:       /usr/bin/cat

%description
%{summary}


%prep
%autosetup -c -T
cp %{SOURCE0} fedora-docs-builder -v
cp %{SOURCE1} . -v


%build
# Nothing here


%install
install -m 0755 -p -D -t %{buildroot}/%{_bindir}/  fedora-docs-builder


%files
%{_bindir}/fedora-docs-builder
%license LICENSE


%changelog
%autochangelog
